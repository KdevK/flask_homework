import socket
import threading

nickname = input('Choose your nickname: ')

host = 'localhost'
port = 5555

clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket.connect((host, port))


def receive():
    while True:
        try:
            message = clientsocket.recv(1024).decode('ascii')
            if message == 'NICK':
                clientsocket.send(nickname.encode('ascii'))
            else:
                print(message)
        except:
            print('An error occurred!')
            clientsocket.close()
            break


def write():
    while True:
        message = f'{nickname}: {input("")}'
        clientsocket.send(message.encode('ascii'))


receive_thread = threading.Thread(target=receive)
receive_thread.start()

write_thread = threading.Thread(target=write)
write_thread.start()
