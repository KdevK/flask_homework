import socket
import threading

host = 'localhost'
port = 5555

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((host, port))
serversocket.listen(100)

clients = []
nicknames = []


def broadcast(message, index):
    # for client in clients:
    #     client.send(message)
    if index % 2 == 1:
        clients[index-1].send(message)
    else:
        if len(clients) >= index + 2:
            clients[index+1].send(message)


def handle(client, index):
    while True:
        message = client.recv(1024)
        broadcast(message, index)


def receive():
    index = -1
    while True:
        client, address = serversocket.accept()
        print(f'Connected with {str(address)}')
        index += 1

        client.send('NICK'.encode('ascii'))
        nickname = client.recv(1024).decode('ascii')
        nicknames.append(nickname)
        clients.append(client)

        print(f'Nickname of the client is {nickname}')
        broadcast(f'{nickname} joined the chat!'.encode('ascii'), index)
        client.send('Connected to the server!'.encode('ascii'))

        thread = threading.Thread(target=handle, args=(client, index))
        thread.start()


print('Server is listening')
receive()
